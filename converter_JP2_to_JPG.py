import cv2
import os
import subprocess
from PIL import Image
from pathlib import Path

os.environ['OPENCV_IO_ENABLE_JASPER'] = '1'

path = Path("/home/konstantin/git/term_papers/IRS990_dataset/")

for entire in path.iterdir():
    if entire.is_dir():
        for image_file in entire.iterdir():
            if image_file.suffix == ".jp2":
                print("FROM: ", image_file)
                print("TO: ", image_file.with_suffix(".jpg"))
                Image.open(str(image_file)).save(str(image_file.with_suffix(".jpg")), "JPEG")
                try:
                    subprocess.Popen(
                        ["rm -rf {}".format(str(image_file))],
                        shell=True
                    )
                except ...:
                    print("No such file")
