import cv2
import numpy as np
import sys

def nothing(x):
  pass

def find_lines_hough(img_for_box_extracing):
    img = cv2.imread(img_for_box_extracing)

    #Увеличение контраста

    # CLAHE (Contrast Limited Adaptive Histogram Equalization)
    clahe = cv2.createCLAHE(clipLimit=3., tileGridSize=(8, 8))

    lab = cv2.cvtColor(img, cv2.COLOR_BGR2LAB)  # convert from BGR to LAB color space
    l, a, b = cv2.split(lab)  # split on 3 different channels

    l2 = clahe.apply(l)  # apply CLAHE to the L-channel

    lab = cv2.merge((l2, a, b))  # merge channels
    img = cv2.cvtColor(lab, cv2.COLOR_LAB2BGR)
    cv2.imwrite("metod_khafa/merge_color.jpg", img)
    #------------------------
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    edges = cv2.Canny(gray, 50, 200, apertureSize=3)
    cv2.imwrite("metod_khafa/edges.jpg", edges)
    # ---------------- Морфологические операции --------------

    kernel_length = np.array(img).shape[1] // 80

    verticle_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, kernel_length))
    hori_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_length, 1))
    img_temp1 = cv2.erode(edges, verticle_kernel, iterations=1)
    img_temp2 = cv2.erode(edges, hori_kernel, iterations=1)
    # Получили два изображения (img_temp1, img_temp2) с выделенными горизонтальными
    # и вертикальными линиями
    # --------------------------------------------------------
    minLineLength = 100
    maxLineGap = 10
    lines_vrtical = cv2.HoughLinesP(img_temp1, 1, np.pi / 180, 20, minLineLength, maxLineGap)
    lines_horizon = cv2.HoughLinesP(img_temp2, 1, np.pi / 180, 20, minLineLength, maxLineGap)

    img2 = img

    for i in range(len(lines_vrtical)):
        for x1, y1, x2, y2 in lines_vrtical[i]:
            cv2.line(img2, (x1, y1), (x2, y2), (0, 255, 0), 2)

    for i in range(len(lines_horizon)):
        for x1, y1, x2, y2 in lines_horizon[i]:
            cv2.line(img2, (x1, y1), (x2, y2), (0, 255, 0), 2)

    cv2.imwrite("metod_khafa/haffa.jpg", img2)


    lines = cv2.HoughLines(edges, 1, np.pi / 180, 200)
    for line in lines:
        for rho, theta in line:
            a = np.cos(theta)
            b = np.sin(theta)
            x0 = a * rho
            y0 = b * rho
            x1 = int(x0 + 1000 * (-b))
            y1 = int(y0 + 1000 * (a))
            x2 = int(x0 - 1000 * (-b))
            y2 = int(y0 - 1000 * (a))
            cv2.line(img2, (x1, y1), (x2, y2), (0, 255, 0), 2)
    cv2.imwrite("/result.jpg", img2)

if __name__ == "__main__":
    find_lines_hough("002.jpg")