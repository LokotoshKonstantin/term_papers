import cv2
import numpy as np
# import pytesseract
from PIL import Image
import sys
import pickle
import os


def box_extraction(img_for_box_extraction_path):
    img = cv2.imread(img_for_box_extraction_path, 0)
    (thresh, img_bin) = cv2.threshold(img, 128, 255,
                                      cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    img_bin = 255 - img_bin

    kernel_length = np.array(img).shape[1] // 50

    verticle_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, kernel_length))
    hori_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_length, 1))
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))

    img_temp1 = cv2.erode(img_bin, verticle_kernel, iterations=1)
    verticle_lines_img = cv2.dilate(img_temp1, verticle_kernel, iterations=1)
    cv2.imwrite("img_temp1.jpg", img_temp1)
    cv2.imwrite("verticle_lines.jpg", verticle_lines_img)

    img_temp2 = cv2.erode(img_bin, hori_kernel, iterations=1)
    horizontal_lines_img = cv2.dilate(img_temp2, hori_kernel, iterations=1)
    cv2.imwrite("img_temp2.jpg", img_temp2)
    cv2.imwrite("horizontal_lines.jpg", horizontal_lines_img)

    alpha = 0.5
    beta = 1.0 - alpha

    img_final_bin = cv2.addWeighted(verticle_lines_img, alpha, horizontal_lines_img, beta, 0.0)
    img_final_bin = cv2.erode(~img_final_bin, kernel, iterations=2)
    (thresh, img_final_bin) = cv2.threshold(img_final_bin, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    cv2.imwrite("img_final.jpg", img_final_bin)

    contours, hierarchy = cv2.findContours(img_final_bin, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_NONE)

    new_contour = []
    for x in range(len(contours) - 1):
        if hierarchy[0][x][3] == -1:
            new_contour.append(contours[x])

    new_contour = new_contour[:-1]
    list_image = []
    return_list_contour = []
    ind = 0
    for c in new_contour:
        x, y, w, h = cv2.boundingRect(c)

        if w > 12 and h > 12:
            new_img = img[y:y + h, x:x + w]
            list_image.append(new_img)
            return_list_contour.append(c)
            cv2.imwrite('/home/konstantin/PycharmProjects/Курсовая_работа/contours/tom_sawyer/test' + str(ind) + '.jpg',
                        new_img)
            ind += 1
            # cv2.drawContours(img, c, -1, (100, 100, 50), 3)
            # cv2.imwrite("img_with_contours.jpg", img)
    return return_list_contour, list_image


# def detect_text_from_image(list_img, lang):
#     ind = 1
#     for x in list_img:
#         text = pytesseract.image_to_string(Image.fromarray(x), lang=lang)
#         print("Text in Contour "+str(ind))
#         print(text)
#         ind += 1


if __name__ == "__main__":
    # with open('data.pickle', 'rb') as f:
    #     data2 = pickle.load(f)
    #
    # list_image = []
    # list_all_contours = []
    # entries = os.listdir("/home/konstantin/PycharmProjects/Курсовая_работа/example/tom_sawyer/")
    # for entry in entries:
    #     list_image.append(entry)
    #
    # list_image.sort()
    # for name in list_image:
    #     list_img = []
    #     t = "/home/konstantin/PycharmProjects/Курсовая_работа/example/tom_sawyer/"+name
    #     list_contour, image_list = box_extraction(t)
    #     for x in list_contour:
    #         x1, y1, w, h = cv2.boundingRect(x)
    #         x2 = x1 + w
    #         y2 = y1 + h
    #         list_img.append([x1, y1, x2, y2])
    #     list_all_contours.append(list_img)
    #
    # # count_error = 0
    # # for x in range(len(data2)):
    # #     if len(data2[x]) != len(list_all_contours[x]):
    # #         count_error += 1
    # #         print(len(list_all_contours[x])-len(data2[x]))
    #
    # metric_error = 0
    # s_all = 0
    # for count_img in range(len(data2)):
    #     metric_error_in_img = 0
    #     s_in_img = 0
    #     data2[count_img] = data2[count_img][::-1]
    #     for count_contour in range(len(data2[count_img])):
    #         evklid = np.sqrt(np.power(data2[count_img][count_contour][0]-list_all_contours[count_img][count_contour][0],2)+np.power(data2[count_img][count_contour][1]-list_all_contours[count_img][count_contour][1], 2))+np.sqrt(np.power(data2[count_img][count_contour][2]-list_all_contours[count_img][count_contour][2],2)+np.power(data2[count_img][count_contour][3]-list_all_contours[count_img][count_contour][3], 2))
    #         s_in_img += abs(data2[count_img][count_contour][0]-data2[count_img][count_contour][2])*abs(data2[count_img][count_contour][1]-data2[count_img][count_contour][3])
    #         metric_error_in_img += evklid
    #     metric_error += metric_error_in_img/len(data2[count_img])
    #     s_all += s_in_img/len(data2[count_img])
    # metric_error = metric_error/len(data2)
    # s_all = s_all/len(data2)
    # print(metric_error)
    # print(s_all)
    # print(metric_error/s_all)
    list_contour, image_list = box_extraction("002.jpg")
    print("Count contours = " + str(len(list_contour)))
    # detect_text_from_image(image_list, sys.argv[2])
