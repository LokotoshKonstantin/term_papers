import torch
from torch.utils.data import Dataset, DataLoader
import torch.nn as nn
from torchvision import transforms, datasets
from matplotlib import pyplot as plt
import h5py
import copy
import time
import os
import glob
import gc
from PIL import Image
import numpy as np

class Rescale(object):
    """Rescale the image in a sample to a given size.

    Args:
        output_size (tuple or int): Desired output size. If tuple, output is
            matched to output_size. If int, smaller of image edges is matched
            to output_size keeping aspect ratio the same.
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        self.output_size = output_size

    def __call__(self, image):
        return transforms.functional.resize(image, self.output_size)

def train_model(model, name, device, dataloader, dataset_sizes, criterion, optimizer, num_epochs=25):
    export_path = './runtime_data/'
    if not os.path.exists(export_path):
        os.mkdir(export_path)

    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 0.0

    train_acc = []
    train_loss = []

    val_acc = []
    val_loss = []

    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                model.train()  # Set model to training mode
            else:
                model.eval()  # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0

            # Iterate over data.
            for inputs, labels in dataloader[phase]:
                inputs = inputs.to(device)
                labels = labels.to(device)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs)
                    _, preds = torch.max(outputs, 1)
                    loss = criterion(outputs, labels)

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)

            epoch_loss = running_loss / dataset_sizes[phase]
            epoch_acc = running_corrects.double() / dataset_sizes[phase]

            if phase == 'train':
                train_acc.append(epoch_acc.cpu().numpy())
                train_loss.append(epoch_loss)
            else:
                val_acc.append(epoch_acc.cpu().numpy())
                val_loss.append(epoch_loss)

            print('{} Loss: {:.4f} Acc: {:.4f}'.format(
                phase, epoch_loss, epoch_acc))

            # deep copy the model
            if phase == 'val' and epoch_acc > best_acc:
                best_acc = epoch_acc
                best_model_wts = copy.deepcopy(model.state_dict())

        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_acc))

    # load best model weights
    model.load_state_dict(best_model_wts)

    # save model
    torch.save(model.state_dict(), export_path + 'model' + name + '.torch')

    # save train statistics
    with h5py.File(export_path + 'stats' + name + '.hdf5', 'w') as f:
        f.create_dataset('train_loss' + name, data=np.array(train_loss), dtype='f')
        f.create_dataset('val_loss' + name, data=np.array(val_loss), dtype='f')
        f.create_dataset('train_acc' + name, data=np.array(train_acc), dtype='f')
        f.create_dataset('val_acc' + name, data=np.array(val_acc), dtype='f')

    return model


def visualize_train_cycle(name):
    export_path = './runtime_data/'

    f = h5py.File(export_path + 'stats' + name + '.hdf5', 'r')
    train_acc = f.get('train_acc' + name)[()]
    train_loss = f.get('train_loss' + name)[()]
    val_acc = f.get('val_acc' + name)[()]
    val_loss = f.get('val_loss' + name)[()]
    f.close()

    title =  name + ' train cycle'

    chart, axes = plt.subplots(1, 2, figsize=(40, 10), gridspec_kw={'hspace': 0})
    axes[0].plot(train_acc, 'r', label='тренировчная')
    axes[0].plot(val_acc, 'g', label='валидационная')
    axes[0].set_ylabel('Значение метрики точности', fontsize=18)
    axes[0].set_xlabel('Количество эпох', fontsize=18)
    axes[0].title.set_text('Кривая метрики точности')
    axes[0].legend()

    axes[1].plot(train_loss, 'r', label='тренировчная')
    axes[1].plot(val_loss, 'g', label='валидационная')
    axes[1].set_ylabel('Значение функции потерь', fontsize=18)
    axes[1].set_xlabel('Количество эпох', fontsize=18)
    axes[1].legend()
    axes[1].title.set_text('Кривая функции потерь')
    plt.savefig(export_path + title + '.png', dpi=96, bbox_inches='tight', pad_inches=0.1)

    plt.cla()
    plt.clf()
    plt.close('all')
    gc.collect()

    
def visualize_model(model, title, scale, device, num_images=6):
    export_path = './runtime_data/'

    test_set = datasets.ImageFolder(root='./markup/test/', transform=transforms.Compose([Rescale(scale), transforms.Grayscale(1), transforms.ToTensor(), transforms.Normalize(0.5, 0.5)]))
    
    test_loader = DataLoader(test_set, batch_size=10, shuffle=True, num_workers=8)
    class_names = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
               'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    
    was_training = model.training
    model.eval()
    images_so_far = 0
    chart, axes = plt.subplots(1, 6, figsize=(15, 10))
    with torch.no_grad():
        for i, (inputs, labels) in enumerate(test_loader):
            inputs = inputs.to(device)
            labels = labels.to(device)

            outputs = model(inputs)
            _, preds = torch.max(outputs, 1)

            for j in range(inputs.size()[0]):
                inp = inputs.cpu().data[j]
                inp = inp.numpy().transpose((1, 2, 0))
                mean = np.array([0.485, 0.456, 0.406])
                std = np.array([0.229, 0.224, 0.225])
                inp = std * inp + mean
                inp = np.clip(inp, 0, 1)
                axes[images_so_far].imshow(inp)
                axes[images_so_far].axis('off')
                axes[images_so_far].set_title('предсказано: {}'.format(class_names[preds[j]]))
                
                images_so_far += 1
                plt.savefig(export_path + title + '_result.png', bbox_inches='tight', pad_inches=0.1)
                if images_so_far == num_images:
                    model.train(mode=was_training)
                    return
                
                
        model.train(mode=was_training) 
   
    
### Custom CNN train loop cycle ###
class CNNModelCustomSet(nn.Module):
    def __init__(self):
        super(CNNModelCustomSet, self).__init__()
        
        # Convolution 1
        self.cnn1 = nn.Conv2d(in_channels=1, out_channels=16, kernel_size=3, stride=1, padding=0)
        self.dropout1 = nn.Dropout2d(0.3)
        self.relu1 = nn.ReLU()
        
        # Max pool 1
        self.maxpool1 = nn.MaxPool2d(kernel_size=2)
     
        # Convolution 2
        self.cnn2 = nn.Conv2d(in_channels=16, out_channels=32, kernel_size=5, stride=1, padding=0)
        self.dropout2 = nn.Dropout2d(0.45)
        self.relu2 = nn.ReLU()
        
        # Max pool 2
        self.maxpool2 = nn.MaxPool2d(kernel_size=2)
        
        # Fully connected 1
        self.fc1 = nn.Linear(256, 26) 
    
    def forward(self, x):
        # Convolution 1
        out = self.cnn1(x)
        out = self.relu1(out)
        out = self.dropout1(out)
        
        # Max pool 1
        out = self.maxpool1(out)
        
        # Convolution 2 
        out = self.cnn2(out)
        out = self.relu2(out)
        out = self.dropout2(out)
        
        # Max pool 2 
        out = self.maxpool2(out)
        
        # flatten
        out = out.view(out.size(0), -1)

        # Linear function (readout)
        out = self.fc1(out)
        
        return out

def evaluate_model(model, dataloader, data_size):
    
    model.eval()   # Set model to evaluate mode
    running_corrects = 0

        # Iterate over data.
    for inputs, labels in dataloader:
        inputs = inputs.to(device)
        labels = labels.to(device)

        outputs = model(inputs)
        _, preds = torch.max(outputs, 1)

        running_corrects += torch.sum(preds == labels.data)

    acc = running_corrects.double() / data_size
    return acc.data

class CNNModelFontsSet(nn.Module):
    def __init__(self):
        super(CNNModelFontsSet, self).__init__()
        
        # Convolution 1
        self.cnn1 = nn.Conv2d(in_channels=1, out_channels=16, kernel_size=3, stride=1, padding=0)
        self.dropout1 = nn.Dropout2d(0.3)
        self.relu1 = nn.ReLU()
        
        # Max pool 1
        self.maxpool1 = nn.MaxPool2d(kernel_size=2)
     
        # Convolution 2
        self.cnn2 = nn.Conv2d(in_channels=16, out_channels=32, kernel_size=5, stride=1, padding=0)
        self.dropout2 = nn.Dropout2d(0.45)
        self.relu2 = nn.ReLU()
        
        # Max pool 2
        self.maxpool2 = nn.MaxPool2d(kernel_size=2)
        
        # Fully connected 1
        self.fc1 = nn.Linear(800, 26) 
    
    def forward(self, x):
        # Convolution 1
        out = self.cnn1(x)
        out = self.relu1(out)
        out = self.dropout1(out)
        
        # Max pool 1
        out = self.maxpool1(out)
        
        # Convolution 2 
        out = self.cnn2(out)
        out = self.relu2(out)
        out = self.dropout2(out)
        
        # Max pool 2 
        out = self.maxpool2(out)
        
        # flatten
        out = out.view(out.size(0), -1)

        # Linear function (readout)
        out = self.fc1(out)
        
        return out