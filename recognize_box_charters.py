import cv2
import os

path = "/home/konstantin/git/term_papers/dataset_without_jp2_with_contours/01-0211497_990T_201006/"

subfolders = [f.path for f in os.scandir(path) if f.is_dir()]
for dir in subfolders:
    print(dir)
    current_image = 0
    with open(dir + "/layout.xml") as f:
        for line in f:
            if "WORD" in line:
                coordinate = line.split("=")[1].split(">")[0].replace("\"", "").split(",")
                x1 = coordinate[0]
                y1 = coordinate[1]
                x2 = coordinate[2]
                y2 = coordinate[3]
                cv2.rectangle(image, (int(x1), int(y1)), (int(x2), int(y2)), (255, 0, 0), 2)

            if "<OBJECT data" in line:
                if current_image == 0:
                    current_image += 1
                    image = cv2.imread(dir + "/00" + str(current_image) + ".jp2")
                else:
                    if current_image < 10:
                        cv2.imwrite(dir + "/00" + str(current_image) + "_test.jpg", image)
                    else:
                        cv2.imwrite(dir + "/0" + str(current_image) + "_test.jpg", image)
                    current_image += 1
                    if current_image < 10:
                        image = cv2.imread(dir + "/00" + str(current_image) + ".jp2")
                    else:
                        image = cv2.imread(dir + "/0" + str(current_image) + ".jp2")
